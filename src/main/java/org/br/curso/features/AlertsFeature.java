package org.br.curso.features;

import br.com.santander.frm.bdd.Feature;
import br.com.santander.frm.bdd.Scenario;

import static br.com.santander.frm.bdd.Gherkin.given_;

import java.util.concurrent.ExecutionException;

@Feature("Alerts")
public class AlertsFeature {
	
	@SuppressWarnings("static-access")
	@Scenario("Disparar Alerts")
	public void disparar_alerts() throws ExecutionException {
		given_("Partindo da tela principal").
		when_("Disparando Alerts").
		then_("Entao tenho Alerts Disparados")
		.execute_();
	}
	
}