package org.br.curso.features;

import br.com.santander.frm.bdd.Feature;
import br.com.santander.frm.bdd.Scenario;

import static br.com.santander.frm.bdd.Gherkin.given_;

import java.util.concurrent.ExecutionException;

@SuppressWarnings("unused")
@Feature("Login ADM")
public class BasicAuthFeature {
	
	@SuppressWarnings("static-access")
	@Scenario("Realizar login")
	public void realizar_login_alert() throws ExecutionException {
		given_("Partindo da tela principal BasicAuth").
		when_("Realizar login no alert").
		then_("Entao terei um Alert Disparados pedindo confirmacao de envio")
		.execute_();
	}
}
