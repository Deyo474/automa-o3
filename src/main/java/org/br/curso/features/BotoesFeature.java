package org.br.curso.features;

import br.com.santander.frm.bdd.Feature;
import br.com.santander.frm.bdd.Scenario;

import static br.com.santander.frm.bdd.Gherkin.given_;

import java.util.concurrent.ExecutionException;

@Feature("Botoes")
public class BotoesFeature {
	
	@SuppressWarnings("static-access")
	@Scenario("Adicionar botoes")
	public void adicionar_botoes() throws ExecutionException {
		given_("Dado que estou na home").
		when_("Adicionar novos botoes").
		then_("Entao tenho botoes cadastrados")
		.execute_();
	}
	
}