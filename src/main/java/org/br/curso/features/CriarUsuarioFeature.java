package org.br.curso.features;

import br.com.santander.frm.bdd.Feature;
import br.com.santander.frm.bdd.Scenario;

import static br.com.santander.frm.bdd.Gherkin.given_;

import java.util.concurrent.ExecutionException;

@Feature("User")
public class CriarUsuarioFeature {
	
	@SuppressWarnings("static-access")
	@Scenario("Criar um novo Usuario")
	public void criar_novo_usuario() throws ExecutionException {
		given_("Dado que eu estou na Pagina automacaocombatista").
		when_("Quando Inserir um novo Usuario").
		then_("Entao tenho um usuario cadastrado")
		.execute_();
	}
	
}