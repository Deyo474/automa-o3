package org.br.curso.features;

import br.com.santander.frm.bdd.Feature;
import br.com.santander.frm.bdd.Scenario;
import static br.com.santander.frm.bdd.Gherkin.given_;

import java.util.concurrent.ExecutionException;

@Feature("DragDrop")
public class DragDropFeature {
	@SuppressWarnings("static-access")
	@Scenario("Mover dragDrop")
	public void mover_drag_and_drop() throws ExecutionException {
		given_("Partindo da tela Principal DragAndDrop").
		when_("Movendo DragAndDrop").
		then_("Terei o DragDrop ocupando o espa�o do quadrado")
		.execute_();
	}
	
}
