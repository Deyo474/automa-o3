package org.br.curso.features;

import br.com.santander.frm.bdd.Feature;
import br.com.santander.frm.bdd.Scenario;

import static br.com.santander.frm.bdd.Gherkin.given_;

import java.util.concurrent.ExecutionException;

@Feature("DropDownSelect")
public class DropDowmSelectFeature {
	
	@SuppressWarnings("static-access")
	@Scenario("Inserir e Validar os campos")
	public void acessar_a_home() throws ExecutionException {
		given_("Partindo da home DropDown").
		and_("Escolher e selecionar todas as op��es do DropDown").
		then_("Entao tenho todas os campos preenchidos")
		.execute_();
	}
	
}