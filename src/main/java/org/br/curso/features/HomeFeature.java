package org.br.curso.features;

import br.com.santander.frm.bdd.Feature;
import br.com.santander.frm.bdd.Scenario;

import static br.com.santander.frm.bdd.Gherkin.given_;

import java.util.concurrent.ExecutionException;

@Feature("Home")
public class HomeFeature {
	
	@SuppressWarnings("static-access")
	@Scenario("Acessar a home")
	public void acessar_a_home() throws ExecutionException {
		given_("Dado que eu estou na Pagina automacaocombatista").
		then_("Entao Valido acesso a pagina")
		.execute_();
	}
	
}
