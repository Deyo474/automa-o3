package org.br.curso.features;

import static br.com.santander.frm.bdd.Gherkin.given_;

import java.util.concurrent.ExecutionException;

import br.com.santander.frm.bdd.Feature;
import br.com.santander.frm.bdd.Scenario;

@Feature("preencher")
public class InputsTextFildFeature {
	
	@SuppressWarnings("static-access")
	@Scenario("Preencher os campos Disponiveis")
	public void preencher_todos_os_campos() throws ExecutionException {
		given_("Partindo da Home").
		when_("Quando preencher todos os campos").
		then_("Entao tenho todos os campos preenchidos")
		.execute_();
	}
	
}
