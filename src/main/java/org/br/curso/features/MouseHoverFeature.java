package org.br.curso.features;

import static br.com.santander.frm.bdd.Gherkin.given_;

import java.util.concurrent.ExecutionException;

import br.com.santander.frm.bdd.Feature;
import br.com.santander.frm.bdd.Scenario;

@Feature("MouseHover")
public class MouseHoverFeature {
	
	@SuppressWarnings("static-access")
	@Scenario("Verificar o MouseHover")
	public void Testar_efeito_MouseHover() throws ExecutionException {
		given_("Partindo da Home MouseHover").
		when_("Quando eu passar o mouse").
		then_("Entao tenho efeito do hover acionado")
		.execute_();
	}
	
}
