package org.br.curso.features;

import static br.com.santander.frm.bdd.Gherkin.given_;

import java.util.concurrent.ExecutionException;

import br.com.santander.frm.bdd.Feature;
import br.com.santander.frm.bdd.Scenario;

@Feature("UploadArquvio")
public class UploadArquivoFeature {
	
	@SuppressWarnings("static-access")
	@Scenario("Fazer upload de arquivo")
	public void realiazar_upload_arquivo() throws ExecutionException {
		given_("Partindo da Home uploadArquivo").
		when_("Quando eu adicionar um arquivo").
		then_("Entao tenho um arquivo adicionado")
		.execute_();
	}
	
}
