package org.br.curso.pages;

import br.com.santander.frm.base.PageBase;
import br.com.santander.frm.base.VirtualElement;
import br.com.santander.frm.exceptions.ElementFindException;
import br.com.santander.frm.exceptions.GenericException;
import br.com.santander.frm.helpers.LoggerHelper;
import static br.com.santander.frm.base.DefaultBaseController.getDriver_;
import static br.com.santander.frm.helpers.QueryHelper.getElementByXPath;

import org.testng.Assert;

public class AlertPage extends PageBase {

	@SuppressWarnings("rawtypes")
	VirtualElement 
					btnClickAlert = getElementByXPath("//button[text()='Clique para JS Alert']"),
					btnClickAlertConfirm = getElementByXPath("//button[text()='Clique para JS Confirm']"),
					btnClickAlertPrompt = getElementByXPath("//button[text()='Clique para JS Prompt']"),
					txtConfirmFirtsAlert = getElementByXPath("//p[text()='Voc� clicou no alerta com sucesso!!']"),  //Confirma o 1 alert
					txtConfirmSecondAlert = getElementByXPath("//p[text()='Voc� clicou: Ok']"),
					txtConfirmThreeAlert = getElementByXPath("//p[text()='Voc� digitou: david']");  //Ele vai gerar um texto com o que for digitado para confirma��o
					
	LoggerHelper logger = new LoggerHelper(AlertPage.class);
	
	
	public void click_alerts() throws ElementFindException, GenericException {
		waitUntilElementExists(btnClickAlert);
		btnClickAlert.click();
		
		getDriver_().switchTo().alert().accept();
		Assert.assertTrue(elementExists(txtConfirmFirtsAlert));
	}
	
	public void click_alert_confirm() throws ElementFindException, GenericException {
		waitUntilElementExists(btnClickAlertConfirm, 10);
		btnClickAlertConfirm.click();
		
		getDriver_().switchTo().alert().accept();
		Assert.assertTrue(elementExists(txtConfirmSecondAlert));
	}
	
	
	public void click_alert_prompt() throws ElementFindException, GenericException {
		waitUntilElementExists(btnClickAlertPrompt, 10);
		btnClickAlertPrompt.click();
		getDriver_().switchTo().alert().sendKeys("david");
		getDriver_().switchTo().alert().accept();
		
		Assert.assertTrue(elementExists(txtConfirmThreeAlert));
	}
	
	
	//Validando o primeiro Alert
//	public String validar_alert_disparado() throws ElementFindException {
//		
//		waitUntilElementExists(txtConfirmFirtsAlert);
//		
//		return txtConfirmFirtsAlert.getText();
//	}
//	
//	public String validar_second_alert() throws ElementFindException {
//		waitUntilElementExists(txtConfirmFirtsAlert);
//		return	txtConfirmFirtsAlert.getText();
//	}
//	
//	public String validar_three_alert() throws ElementFindException {
//		waitUntilElementExists(txtConfirmThreeAlert);
//		return txtConfirmThreeAlert.getText();
//	}
}
