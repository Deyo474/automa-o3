package org.br.curso.pages;

import static br.com.santander.frm.base.DefaultBaseController.getDriver_;
import java.awt.event.KeyEvent;
import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.InputEvent;

import br.com.santander.frm.base.PageBase;
import br.com.santander.frm.exceptions.GenericException;
import br.com.santander.frm.helpers.LoggerHelper;

@SuppressWarnings("unused")
public class BasicAuthPage extends PageBase{

	LoggerHelper logger = new LoggerHelper(BasicAuthPage.class);

	public void interagir_com_alert() throws GenericException, AWTException {
		
		Robot robot = new Robot();
		
		  
		  robot.delay(500);
		 
		  robot.keyPress(KeyEvent.VK_D);
		  robot.keyPress(KeyEvent.VK_A);
		  robot.keyPress(KeyEvent.VK_V);
		  robot.keyPress(KeyEvent.VK_I);
		  robot.keyPress(KeyEvent.VK_D);
		  
		  robot.keyPress(KeyEvent.VK_TAB);
		  robot.delay(500);
		  
		  robot.keyPress(KeyEvent.VK_D);
		  robot.keyPress(KeyEvent.VK_A);
		  robot.keyPress(KeyEvent.VK_V);
		  robot.keyPress(KeyEvent.VK_I);
		  robot.keyPress(KeyEvent.VK_D);
		  robot.delay(500);
		  robot.keyPress(KeyEvent.VK_TAB);
		  robot.keyPress(KeyEvent.VK_ENTER);	  
	}
	
}
