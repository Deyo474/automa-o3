package org.br.curso.pages;

import br.com.santander.frm.base.PageBase;
import br.com.santander.frm.base.VirtualElement;
import br.com.santander.frm.exceptions.ElementFindException;
import br.com.santander.frm.helpers.LoggerHelper;

import static br.com.santander.frm.helpers.QueryHelper.getElementByXPath;

public class BotoesPage extends PageBase{

	@SuppressWarnings("rawtypes")
	VirtualElement
					btnRaised = getElementByXPath("//a[@id='teste']"),
					btnFloating = getElementByXPath("//a[@class='btn-floating btn-large waves-light red']"),
					btnFlat = getElementByXPath("//a[@class='waves-teal btn-flat']"),
					btnSummit = getElementByXPath("//button[@class='btn waves-light']"),
					txtValidarBotao = getElementByXPath("//h5[text()='Voc� Clicou no Bot�o!']");
	
	LoggerHelper logger = new LoggerHelper(BotoesPage.class);
	
	
	public void adicionar_botoes() throws ElementFindException {
		waitUntilElementExists(btnRaised, 5);
		btnRaised.click();
		
		waitUntilElementExists(btnFloating, 5);
		btnFloating.click();
		
		waitUntilElementExists(btnFlat, 5);
		btnFlat.click();
		
		waitUntilElementExists(btnSummit, 5);
		btnSummit.click();
		
		
	}
	
	public String validar_botoes_adicionado() throws ElementFindException {
		waitUntilElementExists(txtValidarBotao, 10);
		
		return txtValidarBotao.getText();
		
	}
}
