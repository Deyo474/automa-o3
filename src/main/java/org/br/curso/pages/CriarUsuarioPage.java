	package org.br.curso.pages;

import br.com.santander.frm.exceptions.ElementFindException;
import br.com.santander.frm.helpers.LoggerHelper;
import br.com.santander.frm.base.*;
import static br.com.santander.frm.helpers.QueryHelper.getElementByXPath;
import static br.com.santander.frm.helpers.QueryHelper.getElementById;
import org.testng.Assert;

@SuppressWarnings("unused")
public class CriarUsuarioPage extends PageBase{

	@SuppressWarnings("rawtypes")
	VirtualElement 
				   pnlNome = getElementByXPath("//input[@id='user_name']"),
				   pnlUltimoNome = getElementByXPath("//input[@id='user_lastname']"),
				   pnlEmail = getElementByXPath("//input[@id='user_email']"),
				   pnlEndereco = getElementByXPath("//input[@id='user_address']"),
				   pnlUniversidade = getElementByXPath("//input[@id='user_university']"),
				   pnlProfissao = getElementByXPath("//input[@id='user_profile']"),
				   pnlGenero = getElementByXPath("//input[@id='user_gender']"),
				   pnlIdade = getElementByXPath("//input[@id='user_age']"),
				   btnCriarUsuarioPreenchido = getElementByXPath("//input[@name='commit']"),
				   btnListaDeUsuarios = getElementByXPath("//a[text()='Lista de Usu�rios']"),
				   txtValidarUsuarioCadastrado = getElementByXPath("//p[@id='notice']");
				   
	
	LoggerHelper logger = new LoggerHelper(CriarUsuarioPage.class);
	
	
	               
	//M�todo para adicionar um usuario
	public void preencher_login(String nome, String ultimoNome, String email, String endereco,
			String universidade, String profissao, String genero, String idade)
					throws ElementFindException {
		
		logger.info("Adicioanando um usuario", true);	
		//Inserir Todas as informa��es
		
			//trocar por wait
		if (elementExists(pnlNome) && elementExists(pnlUltimoNome) && elementExists(pnlEmail) && elementExists(pnlEndereco)
				&& elementExists(pnlUniversidade) &&
				elementExists(pnlProfissao) && elementExists(pnlGenero)
				&& elementExists(pnlIdade)) {
			
		pnlNome.sendKeys(nome);
		pnlUltimoNome.sendKeys(ultimoNome);
		pnlEmail.sendKeys(email);
		pnlEndereco.sendKeys(endereco);
		pnlUniversidade.sendKeys(universidade);
		pnlProfissao.sendKeys(profissao);
		pnlGenero.sendKeys(genero);
		pnlIdade.sendKeys(idade);
		
		logger.info("Campos da Cria��o de usuario preenchidos", true);
		
		
		//apertar em criar ap�s preencher todos os dados;
		btnCriarUsuarioPreenchido.click();
		
		}
		
	}
	
	
	public String validar_usuario_cadastrado() throws ElementFindException {
		waitUntilElementExists(txtValidarUsuarioCadastrado, 10);
		
		
		return txtValidarUsuarioCadastrado.getText();
	}
	
}