package org.br.curso.pages;

import br.com.santander.frm.base.PageBase;
import br.com.santander.frm.base.VirtualElement;
import br.com.santander.frm.exceptions.ElementFindException;
import br.com.santander.frm.exceptions.GenericException;
import br.com.santander.frm.helpers.LoggerHelper;
import static br.com.santander.frm.base.DefaultBaseController.getDriver_;
import static br.com.santander.frm.helpers.QueryHelper.getElementByXPath;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class DragDropPage extends PageBase{
	
	@SuppressWarnings("rawtypes")
	VirtualElement   imgFace = getElementByXPath("//img[@id='winston']"),
					 divQuadrado = getElementByXPath("//div[@id='dropzone']");
	
	
	LoggerHelper logger = new LoggerHelper(DragDropPage.class);
	
	
	
	public void mexer_a_imagem_para_o_quadrado() throws ElementFindException, GenericException{
		Actions action = new Actions(getDriver_());
		action.dragAndDrop(imgFace.get(), divQuadrado.get()).build().perform();
		
		
		waitUntilElementExists(imgFace);
		imgFace.isSelected();
	}
	
	
	public void validar_imagem_no_quadrado() throws GenericException {
		//Como validar ?
		

		
		
	    //int posicaoInical = imgFace.get.getPositionX();

	}
	
}
