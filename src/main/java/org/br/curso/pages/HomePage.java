package org.br.curso.pages;

import br.com.santander.frm.exceptions.ElementFindException;
import br.com.santander.frm.helpers.LoggerHelper;
import br.com.santander.frm.base.*;
import static br.com.santander.frm.helpers.QueryHelper.getElementByXPath;
import static br.com.santander.frm.helpers.QueryHelper.getElementById;
import org.testng.Assert;

@SuppressWarnings("unused")
public class HomePage extends PageBase{

	@SuppressWarnings("rawtypes")
	VirtualElement 
			txtHome = getElementByXPath("//h5[@class='orange-text center ']"),
			btnFormulario = getElementByXPath("//a[@class='collapsible-header ']"),
			btnCriarUsuario = getElementByXPath("//a[text()='Criar Usu�rios']"),
	        btnBuscaDeElementos = getElementByXPath("//a[text()='Busca de elementos']"),
			btnInputsFilds = getElementByXPath("//a[text()='Inputs e TextField']"),
			btnBotoes = getElementByXPath("//a[text() = 'Bot�es']"),
			btnMudancaDeFoco = getElementByXPath("//a[text()='Mudan�a de foco']"),
			btnAlert = getElementByXPath("//a[text()='Alert']"),
			btnIteracoes = getElementByXPath("//a[text()='Itera��es']"),
			btnDragAndDrop = getElementByXPath("//a[text()='Drag And Drop']"),
			btnMouseHover = getElementByXPath("//a[text()='Mousehover']"),
			btnOutros = getElementByXPath("//a[text()='Outros']"),
			btnUploadArquivos = getElementByXPath("//a[text()='Upload de Arquivo']"),
			btnBasicAuth = getElementByXPath("//a[text()='Basic Auth(user:admin, password: admin)']"),
			btnDropDowmSelect = getElementByXPath("//a[text()='Dropdown e Select']");
	
	
	LoggerHelper logger = new LoggerHelper(HomePage.class);
	
	public void validar_pagina() {
		
		Assert.assertTrue(elementExists(txtHome));
		logger.info("Pagina de Home encontrada com Sucesso!");
		
	}
	
	public void click_btn_formulario() throws ElementFindException {
		waitUntilElementExists(btnFormulario);
		btnFormulario.click();
	}
	public void click_btn_cria_usuario() throws ElementFindException {
		waitUntilElementExists(btnCriarUsuario);
		btnCriarUsuario.click();
	}
	
	public void click_busca_de_elementos() throws ElementFindException {
		// TODO Auto-generated method stub
		waitUntilElementExists(btnBuscaDeElementos);
		btnBuscaDeElementos.click();
	}
	
	
	public void click_inputs_textfild() throws ElementFindException {
		waitUntilElementBeClickable(btnInputsFilds);
		btnInputsFilds.click();
	}
	
	public void click_botoes() throws ElementFindException {
		waitUntilElementBeClickable(btnBotoes);
		btnBotoes.click();
		
	}
	
	public void click_mudanca_de_foco() throws ElementFindException {
		waitUntilElementExists(btnMudancaDeFoco);
		btnMudancaDeFoco.click();
		
	}
	
	public void click_alert() throws ElementFindException {
		waitUntilElementBeClickable(btnAlert);
		btnAlert.click();
	}
	
	public void apertar_em_iteracoes() throws ElementFindException {
		waitUntilElementExists(btnIteracoes);
		btnIteracoes.click();
	}
	
	public void apertar_em_DragAndDrop() throws ElementFindException {
		waitUntilElementBeClickable(btnDragAndDrop);
		btnDragAndDrop.click();
	}
	
	public void apertar_em_mouseHover() throws ElementFindException {
		waitUntilElementBeClickable(btnMouseHover);
		btnMouseHover.click();
	}
	
	public void apertar_em_Outros() throws ElementFindException {
		
		waitUntilElementExists(btnOutros);
		btnOutros.click();
	}
	
	public void apertar_em_upload() throws ElementFindException {
		waitUntilElementBeClickable(btnUploadArquivos);
		btnUploadArquivos.click();
	}
	
	public void apertar_BasicAuth() throws ElementFindException {
		waitUntilElementBeClickable(btnBasicAuth);
		btnBasicAuth.click();
	}
	
	public void apertar_em_dropDownSelect() throws ElementFindException {
		waitUntilElementBeClickable(btnDropDowmSelect);
		btnDropDowmSelect.click();
	}

}
