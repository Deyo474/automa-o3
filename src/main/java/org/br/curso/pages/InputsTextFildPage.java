package org.br.curso.pages;

import br.com.santander.frm.base.PageBase;
import br.com.santander.frm.base.VirtualElement;
import br.com.santander.frm.exceptions.ElementFindException;
import br.com.santander.frm.helpers.LoggerHelper;
import static br.com.santander.frm.helpers.QueryHelper.getElementByXPath;

public class InputsTextFildPage  extends PageBase{

	@SuppressWarnings("rawtypes")
	VirtualElement
				  txtFirstName = getElementByXPath("//input[@id='first_name']"),
				  txtLastName = getElementByXPath("//input[@id='last_name']"),
				  txtPassword = getElementByXPath("//input[@id='password']"),
				  txtEmail = getElementByXPath("//input[@id='email']"),
				  txtArea = getElementByXPath("//textarea[@id='textarea1']");
				  
	
	LoggerHelper logger = new LoggerHelper(InputsTextFildPage.class);
	
	public void preencher_os_campos(String nome, String ultimoNome, String password,
			String email, String textArea)  throws ElementFindException{
		
		
		if (elementExists(txtFirstName) && elementExists(txtLastName)
				&& elementExists(txtPassword) && elementExists(txtEmail)
				&& elementExists(txtArea)) {
		
			txtFirstName.sendKeys(nome);
			txtLastName.sendKeys(ultimoNome);
			txtPassword.sendKeys(password);
			txtEmail.sendKeys(email);
			txtArea.sendKeys(textArea);
			
			logger.info("Campos Foram preenchidos com sucesso!", true);
		
		}
	}
}
