package org.br.curso.pages;
import br.com.santander.frm.base.PageBase;
import br.com.santander.frm.base.VirtualElement;
import br.com.santander.frm.exceptions.ElementFindException;
import br.com.santander.frm.helpers.LoggerHelper;

import static br.com.santander.frm.helpers.QueryHelper.getElementByXPath;
@SuppressWarnings("unused")
public class MouseHoverPage extends PageBase{
	
	@SuppressWarnings("rawtypes")
	VirtualElement 
					MouseHover = getElementByXPath("//p[@class='activator']"),
					txtMouseHover = getElementByXPath("//p[text()='Voc� usou o mouse hover!']");
					
	
	
	LoggerHelper logger = new LoggerHelper(MouseHoverPage.class);
	
	
	public void arrastar_o_mouse() throws Exception {
		
		waitUntilElementExists(MouseHover, 5);
		MouseHover.mouseOver();
	}
	
	public String verificar_mouseHover() throws ElementFindException {
		waitUntilElementExists(txtMouseHover, 10);
		return txtMouseHover.getText();
	}
}
