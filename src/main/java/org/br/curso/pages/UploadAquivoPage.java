package org.br.curso.pages;

import br.com.santander.frm.base.PageBase;
import br.com.santander.frm.base.VirtualElement;
import br.com.santander.frm.exceptions.ElementFindException;
import br.com.santander.frm.helpers.LoggerHelper;

import static br.com.santander.frm.helpers.QueryHelper.getElementByXPath;

import org.testng.Assert;
public class UploadAquivoPage extends PageBase{

	@SuppressWarnings("rawtypes")
	VirtualElement 
	
				inpInserirUpload = getElementByXPath("//input[@id='upload']"),
				imgValidarOpload = getElementByXPath("//img[@id='thumbnail']");

	
	LoggerHelper logger = new LoggerHelper(UploadAquivoPage.class);
	
	
	public void inserir_uploado() throws ElementFindException {
		inpInserirUpload.sendKeys("C:\\Users\\David Cardoso\\Desktop\\imagens\\olhaisso.png");
	}
	
	public void validar_insercao_uploado() {
		Assert.assertTrue(elementExists(imgValidarOpload));
		logger.info("Foi Inserido!");
	}
}
	