package org.br.curso.steps;

import org.br.curso.pages.AlertPage;
import org.br.curso.pages.HomePage;

import br.com.santander.frm.bdd.Step;
import br.com.santander.frm.exceptions.ElementFindException;
import br.com.santander.frm.exceptions.GenericException;

import static br.com.santander.frm.base.DefaultBaseController.getPage_;
public class AlertsSteps {

	AlertPage alertPage = getPage_(AlertPage.class);
	HomePage homePage = getPage_(HomePage.class);
	
	@Step("Partindo da tela principal")
	public void Partindo_da_tela_principal() {
		homePage.validar_pagina();
	}
	
	@Step("Disparando Alerts")
	public void Disparando_Alerts() throws ElementFindException, GenericException {
		homePage.click_mudanca_de_foco();
		homePage.click_alert();
		
		alertPage.click_alerts();
		//alertPage.validar_alert_disparado();
		
		alertPage.click_alert_confirm();
		//alertPage.validar_second_alert();
		
		alertPage.click_alert_prompt();
		//alertPage.validar_three_alert();
	}
	
	@Step("Entao tenho Alerts Disparados")
	public void Entao_tenho_Alerts_Disparados() throws ElementFindException {
		//alertPage.validar_alert_disparado();
	}
}
