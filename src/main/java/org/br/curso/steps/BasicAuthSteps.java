package org.br.curso.steps;

import static br.com.santander.frm.base.DefaultBaseController.getPage_;

import java.awt.AWTException;

import org.br.curso.pages.BasicAuthPage;
import org.br.curso.pages.HomePage;

import br.com.santander.frm.bdd.Step;
import br.com.santander.frm.exceptions.ElementFindException;
import br.com.santander.frm.exceptions.GenericException;

public class BasicAuthSteps {

	HomePage homePage = getPage_(HomePage.class);
	BasicAuthPage basicAuthPage = getPage_(BasicAuthPage.class);
	
	@Step("Partindo da tela principal BasicAuth")
	public void Partindo_da_tela_principal_BasicAuth() {
		homePage.validar_pagina();
	}
	
	@Step("Realizar login no alert")
	public void Realizar_login_no_alert() throws ElementFindException, GenericException, AWTException {
		homePage.apertar_em_Outros();
		homePage.apertar_BasicAuth();
		
		basicAuthPage.interagir_com_alert();
	}
	
	
	@Step("Entao terei um Alerts Disparados pedindo confirmacao de envio")
	public void Entao_terei_um_Alert_Disparados_pedindo_confirmacao_de_envio() {
		
	}
}
