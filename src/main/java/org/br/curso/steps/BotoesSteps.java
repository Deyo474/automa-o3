package org.br.curso.steps;

import org.br.curso.pages.BotoesPage;
import org.br.curso.pages.HomePage;

import static br.com.santander.frm.base.DefaultBaseController.getPage_;
import br.com.santander.frm.bdd.Step;
import br.com.santander.frm.exceptions.ElementFindException;

public class BotoesSteps{

	BotoesPage botoesPage = getPage_(BotoesPage.class);
	HomePage homePage = getPage_(HomePage.class);
	
	@Step("Dado que estou na home")
	public void Dado_que_estou_na_home() {
		homePage.validar_pagina();
	}
	
	@Step("Adicionar novos botoes")
	public void Adicionar_novos_botoes() throws ElementFindException {
		homePage.click_busca_de_elementos();
		homePage.click_botoes();
		
		botoesPage.adicionar_botoes();
	}
	
	@Step("Entao tenho botoes cadastrados")
	public void Entao_tenho_botoes_cadastrados() throws ElementFindException {
		botoesPage.validar_botoes_adicionado();
	}
	
}
