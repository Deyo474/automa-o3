package org.br.curso.steps;

import static br.com.santander.frm.base.DefaultBaseController.getPage_;

import org.apache.commons.math3.stat.inference.GTest;
import org.br.curso.pages.CriarUsuarioPage;
import org.br.curso.pages.HomePage;

import br.com.santander.frm.bdd.Step;
import br.com.santander.frm.exceptions.ElementFindException;
import static br.com.santander.frm.helpers.DataTableHelper.getDt_;

@SuppressWarnings("unused")
public class CriarUsuarioSteps {
	HomePage homePage = getPage_(HomePage.class);
	CriarUsuarioPage criaPage = getPage_(CriarUsuarioPage.class);
	
	@Step("Dado que eu estou na Pagina automacaocombatista")
	public void Validando_home() throws ElementFindException {
		homePage.validar_pagina();

	}
	
	@Step("Quando Inserir um novo Usuario")
	public void Quando_Inserir_um_novo_Usuario() throws ElementFindException {
		homePage.click_btn_formulario();
		homePage.click_btn_cria_usuario();
	}
	
	@Step("Entao tenho um usuario cadastrado")
	public void Entao_tenho_um_usuario_cadastrado() throws ElementFindException {
		criaPage.preencher_login(getDt_().getStringOf("nome"), getDt_().getStringOf("ultimo_nome"),
				getDt_().getStringOf("email"), getDt_().getStringOf("endereco"), getDt_().getStringOf("universidade"),
				getDt_().getStringOf("profissao"), getDt_().getStringOf("genero"), getDt_().getStringOf("idade"));
	}
}
