package org.br.curso.steps;

import org.br.curso.pages.DragDropPage;
import org.br.curso.pages.HomePage;

import br.com.santander.frm.bdd.Step;
import br.com.santander.frm.exceptions.ElementFindException;
import br.com.santander.frm.exceptions.GenericException;

import static br.com.santander.frm.base.DefaultBaseController.getPage_;

public class DragDropSteps {

	HomePage homePage = getPage_(HomePage.class);
	DragDropPage dragDropPage = getPage_(DragDropPage.class);	
	
	@Step("Partindo da tela Principal DragAndDrop")
	public void Partindo_da_tela_Principal_DragAndDrop() {
		homePage.validar_pagina();
	}
	
	@Step("Movendo DragAndDrop")
	public void Movendo_DragAndDrop() throws ElementFindException, GenericException {
		homePage.apertar_em_iteracoes();
		homePage.apertar_em_DragAndDrop();
		
		dragDropPage.mexer_a_imagem_para_o_quadrado();
	}
	
	@Step("Terei o DragDrop ocupando o espa�o do quadrado")
	public void Terei_o_DragDrop_ocupando_o_espa�o_do_quadrado() {
		
	}
}
