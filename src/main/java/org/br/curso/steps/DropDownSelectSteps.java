package org.br.curso.steps;

import static br.com.santander.frm.base.DefaultBaseController.getPage_;

import org.br.curso.pages.HomePage;

import br.com.santander.frm.bdd.Step;
import br.com.santander.frm.exceptions.ElementFindException;

public class DropDownSelectSteps {

	HomePage homePage = getPage_(HomePage.class);
	
	@Step("Partindo da home DropDown")
	public void Partindo_da_home_DropDown() {
		homePage.validar_pagina();
	}
	
	@Step("Escolher e selecionar todas as op��es do DropDown")
	public void Escolher_e_selecionar_todas_as_op��es_do_DropDown() throws ElementFindException {
		homePage.click_busca_de_elementos();
		homePage.apertar_em_dropDownSelect();
	}
	
	@Step("Entao tenho todas os campos preenchidos")
	public void Entao_tenho_todas_os_campos_preenchidos() {
		
	}
	
	
}
