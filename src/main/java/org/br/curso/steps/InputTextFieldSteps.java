package org.br.curso.steps;

import static br.com.santander.frm.base.DefaultBaseController.getPage_;

import org.br.curso.pages.HomePage;
import org.br.curso.pages.InputsTextFildPage;

import br.com.santander.frm.bdd.Step;
import br.com.santander.frm.exceptions.ElementFindException;
import static br.com.santander.frm.helpers.DataTableHelper.getDt_;
import org.apache.commons.math3.stat.inference.GTest;
@SuppressWarnings("unused")
public class InputTextFieldSteps {
	HomePage homePage = getPage_(HomePage.class);
	
	InputsTextFildPage inputsfild = getPage_(InputsTextFildPage.class);
	
	
	@Step("Patindo da Home")
	public void Patindo_da_Home() {
		homePage.validar_pagina();
	}
	
	@Step("Quando preencher todos os campos")
	public void Quando_preencher_todos_os_campos() throws ElementFindException {
		homePage.click_busca_de_elementos();
		homePage.click_inputs_textfild();
		
		inputsfild.preencher_os_campos(getDt_().getStringOf("nome"), getDt_().getStringOf("ultimo_nome"),
				getDt_().getStringOf("password"), getDt_().getStringOf("email"),getDt_().getStringOf("Textarea"));
		
	}
	
	@Step("Entao tenho todos os campos preenchidos")
	public void Entao_tenho_todos_os_campos_preenchidos() {
		
	}
	
}
