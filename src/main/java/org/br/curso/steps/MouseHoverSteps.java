package org.br.curso.steps;

import org.br.curso.pages.HomePage;
import org.br.curso.pages.MouseHoverPage;
import static br.com.santander.frm.base.DefaultBaseController.getPage_;
import br.com.santander.frm.bdd.Step;

public class MouseHoverSteps {
	
	MouseHoverPage mouseHoverPage = getPage_(MouseHoverPage.class);
	HomePage homePage = getPage_(HomePage.class);
	
	@Step("Partindo da Home MouseHover")
	public void Partindo_da_Home_MouseHover() {
		
	}
	
	@Step("Quando eu passar o mouse")
	public void Quando_eu_passar_o_mouse() throws Exception {
		homePage.apertar_em_iteracoes();
		homePage.apertar_em_mouseHover();
		mouseHoverPage.arrastar_o_mouse();
	}
	
	@Step("Entao tenho efeito do :hover acionado")
	public void Entao_tenho_efeito_do_hover_acionado(){
		
	}
}
