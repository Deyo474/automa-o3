package org.br.curso.steps;

import static br.com.santander.frm.base.DefaultBaseController.getPage_;

import org.br.curso.pages.HomePage;
import org.br.curso.pages.UploadAquivoPage;

import br.com.santander.frm.bdd.Step;
import br.com.santander.frm.exceptions.ElementFindException;

public class UploadArquivoSteps {

	HomePage homePage = getPage_(HomePage.class);
	UploadAquivoPage uploadArquivoPage  = getPage_(UploadAquivoPage.class);
	
	
	@Step("Partindo da Home uploadArquivo")
	public void Partindo_da_Home_uploadArquivo() {
		homePage.validar_pagina();
	}
	
	@Step("Quando eu adicionar um arquivo")
	public void Quando_eu_adicionar_um_arquivo() throws ElementFindException {
		homePage.apertar_em_Outros();
		homePage.apertar_em_upload();
		
		uploadArquivoPage.inserir_uploado();
		
	}
	
	@Step("Entao tenho um arquivo adicionado")
	public void Entao_tenho_um_arquivo_adicionado() {
		uploadArquivoPage.validar_insercao_uploado();
	}
}
